#!/bin/sh

# depends:
# curl netcat jq fzf

CACHE=${XDG_CACHE_DIR:-$HOME/.cache}/spotify
mkdir -p $CACHE
API=https://api.spotify.com/v1

################################################################################
# AUTH
################################################################################

. $CACHE/spotify.env
OAUTH_APPLICATION_ROOT="$CACHE"
OAUTH_CLIENT_SCOPES="\
user-read-playback-state%20\
user-modify-playback-state%20\
user-read-currently-playing%20\
user-library-read"
OAUTH_AUTHORIZE_ENDPOINT='https://accounts.spotify.com/authorize'
OAUTH_TOKEN_REQ_ENDPOINT='https://accounts.spotify.com/api/token'

. oauth.sh

################################################################################

test -r $CACHE/device_sel && . $CACHE/device_sel

FZF='fzf --no-sort --with-nth=2..'

REQUEST ()
{
    TYPE=$1
    WHAT=$2
    DATA=$3
    [ -z "$DATA" ] && DATA="{}"
    if ! [ $TYPE = GET ] ; then
        DATAFL=--data
    fi
    URL=$API/$WHAT
    curl $CURLOPTS -L -s --request $TYPE \
         --url $URL \
         --header "Authorization: Bearer $OAUTH_TOKEN" \
         --header "Content-Type: application/json" \
         $DATAFL "$DATA"
}

request_stop ()
{
    REQUEST PUT me/player/pause
}

request_play ()
{
    test -n "$ACTIVE_DEVICE" && DATA="?device_id=$ACTIVE_DEVICE"
    test -n "$1" && CONTEXT_URI="{ \"context_uri\": \"$1\" }"
    echo "$CONTEXT_URI"
    REQUEST PUT "me/player/play$DATA" "$CONTEXT_URI"
}

request_next ()
{
    REQUEST POST me/player/next
}

request_previous ()
{
    REQUEST POST me/player/previous
}

request_status ()
{
    RESP="$(REQUEST GET me/player)"
    PLAYING=$(echo $RESP | jq -r '.is_playing')
    case $PLAYING in
        true) echo $RESP | jq -r '.item.name' ;;
        *) echo "not playing" ;;
    esac
}

request_device ()
{
    REQUEST GET me/player/devices  | {
        jq -r ".devices | .[] | .id, .name, .is_active"
        echo EOF
    } | while :
    do
        read id; read name; read active
        test "$id" = EOF && break
        case $active in
            true) active='*' ;;
            *) active=' ' ;;
        esac
        echo "$id $active $name"
    done | $FZF | tee /dev/stderr | cut -d' ' -f1 | {
        read selected
        REQUEST PUT me/player "{\"device_ids\":[\"$selected\"]}"
        echo "ACTIVE_DEVICE=$selected" > $CACHE/device_sel
    }
}

fetch_album_list ()
{
    lim=50
    i=0
    while REQUEST GET "me/albums?limit=$lim&offset=$i" \
            | jq -r '.items
| if . == [] then ""|halt_error else .[] end
| .album | .id, .artists[0].name, .name, .total_tracks, (
    .tracks.items | .[] | .id, .name
)'
    do
        i=$((lim + i))
    done
    echo EOF
}

request_refresh_album ()
{
    rm $CACHE/albums/* 2>/dev/null || mkdir -p $CACHE/albums
    fetch_album_list | while :
    do
        read album_id; read artist_name; read album_name; read total_tracks
        test "$album_id" = "EOF" && break
        filename=$(echo "${artist_name}_${album_name}_${album_id}" \
                   | tr -dc '[:alnum:]_')
        echo "$album_name"
        {
            echo "$album_id $artist_name $album_name"
            for i in `seq $total_tracks`
            do
                read track_id ; read track_name
                echo "$track_id $track_name"
            done
        } >$CACHE/albums/$filename
    done
}

select_album ()
{
    case $#$1 in
        1-r) PICK="cat" ;;
        0)   PICK="$FZF" ;;
        *-r) PICK="$FZF -f $@" ;;
        *)   PICK="$FZF -q $@" ;;
    esac

    head $CACHE/albums/* -n 1 -q \
        | sort -R | $PICK | head -n 1
}

request_queue_album ()
{
    pick=$(select_album "$@")
    [ -z "$pick" ] && {
        echo "no match" > /dev/stderr
        return 1
    }
    echo "$pick"
    if [ -f $CACHE/queue ] && [ "$(tail -n 1 $CACHE/queue)" = "$pick" ] ; then
        echo "skipping duplicate" > /dev/stderr
    else
        echo "$pick" >> $CACHE/queue
    fi
}

request_choose_album ()
{
    pick=$(select_album "$@")
    echo "$pick"
    [ -z "$pick" ] && {
        echo "no match"
        exit 1
    }
    request_play "spotify:album:$(echo $pick | cut -d ' ' -f 1)"
}

request_queue_get ()
{
    [ -r $CACHE/queue ] && cat $CACHE/queue
    REQUEST GET "me/player/queue" \
        | jq -r ' .queue
| map(@text "[\(.album.name)][\(.track_number)][\(.name)]")
| @sh' | xargs -n 1 echo
}

request_queue_clear ()
{
    rm -f $CACHE/queue
}

quiet_queue_pop ()
{
    sed -i '1d' $CACHE/queue
}

request_queue_pop ()
{
    quiet_queue_pop
    cat $CACHE/queue
}

request_queue_undo ()
{
    sed -i '$d' $CACHE/queue
    cat $CACHE/queue
}

request_queue_rewrite ()
{
    case $1 in
        -dd) set -x; debug=true ;;
        -d) set -x ;;
        *)  debug=false ;;
    esac
    test -r $CACHE/queue && next=$(head -n1 $CACHE/queue)
    test -z "$next" && next=`select_album -r`
    next=$(echo "$next" | cut -d' ' -f1)

    REQUEST GET "me/player" 2>/dev/null \
        | jq -r '.item | .album.total_tracks, .track_number, .id' \
        | {
        read total_tracks; read track_number; read item_id
        test -n "$track_number" || exit 0
        if ! $debug
        then
            test "$total_tracks" -ne "$track_number" && return 0
            grep -Fx "$item_id" $CACHE/rewrite >/dev/null 2>&1 && return 0
        fi
        tail -n +2 $CACHE/albums/*_$next | cut -d' ' -f1 | while read track
        do
            REQUEST POST "me/player/queue?uri=spotify:track:$track"
        done
        echo "$item_id" >$CACHE/rewrite
        printf "current %2d/%2d %d adding %d" \
               $track_number $total_tracks $item_id $next >>~/log/rewrite
        request_queue_pop
    }
}

if [ "$#" -eq "0" ] ; then
    request_status
    exit 0
fi

if [ "$1" = -r ] ; then
    [ -z "$2" ] && exit 1
    FMT="@text \"$2\""
    REQUEST GET me/player | jq -j "$FMT"
    request_queue_rewrite >>$HOME/log/rewrite
    exit 0
fi

if [ "$1" = -q ]; then
    REQUEST GET $2
    exit 0
fi

requests="\
stop
play
next
previous
status
device
refresh album
queue album
choose album
queue get
queue clear
queue pop
queue undo
queue rewrite"

args=$1
while [ "$#" -gt "0" ] ; do
    pattern="$(echo "$args" | sed 's/ /\\w* /g')\w*"
    match="$(echo "$requests" | grep "^$pattern")"
    shift; args="$args $1"
    if [ "$(echo "$match" | wc -l)" -eq "1" ] && ! [ -z "$match" ] ; then
        request_$(echo $match | tr ' ' '_') "$@"
        exit $?
    fi
done

printf "==> input matched:\n${match}\n==> available:\n$requests\n" 2>&1

exit 1
